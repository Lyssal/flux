install:
	composer install
	yarn install
	./node_modules/gulp/bin/gulp.js

serve:
	php -S 127.0.0.1:8000 -t public

cache-clear-dev:
	bin/console cache:clear --no-warmup --env=dev
	bin/console cache:warmup --env=dev

cache-clear-prod:
	bin/console cache:clear --no-warmup --env=prod
	bin/console cache:warmup --env=prod

cache-clear:
	make cache-clear-prod
	make cache-clear-dev

asset-watch:
	./node_modules/gulp/bin/gulp.js
	./node_modules/gulp/bin/gulp.js watch

import:
	bin/console app:feed:import
