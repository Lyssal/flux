# Flux


[![SensioLabsInsight](https://insight.sensiolabs.com/projects/97e615b2-0e84-4326-bcac-6c4fab2bab2a/small.png)](https://insight.sensiolabs.com/projects/97e615b2-0e84-4326-bcac-6c4fab2bab2a)


Flux is an application which read feeds and save each new article in database.

![Flux](doc/flux.png)

See the Makefile for commands.
