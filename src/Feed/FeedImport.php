<?php
namespace App\Feed;

use App\Entity\Feed;
use App\Entity\FeedItem;
use App\Exception\FeedException;
use DateTime;
use Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager;
use Lyssal\Text\SimpleString;
use SimplePie;
use SimplePie_Item;

/**
 * Import the new feed items.
 */
class FeedImport
{
    private $entityAdministratorManager;

    /**
     * @var string The cache directory
     */
    private $cacheDir;

    public function __construct(EntityAdministratorManager $entityAdministratorManager, string $cacheDir)
    {
        $this->entityAdministratorManager = $entityAdministratorManager;
        $this->cacheDir = $cacheDir;
    }


    /**
     * Import all the new feed items.
     */
    public function importAll()
    {
        $feeds = $this->entityAdministratorManager->get(Feed::class)->findAll();

        foreach ($feeds as $feed) {
            $this->importFeed($feed);
        }
    }

    /**
     * Import one feed.
     *
     * @param \App\Entity\Feed $feed The feed
     *
     * @return int The count of saved items
     *
     * @throws \App\Exception\FeedException If there an error with SimplePie
     */
    public function importFeed(Feed $feed)
    {
        if (!$feed->canImport()) {
            return 0;
        }

        $itemCount = 0;
        $feedReader = new SimplePie();
        $feedReader->set_cache_location($this->cacheDir);
        $feedReader->set_feed_url($feed->getUrl());
        $feedReader->set_stupidly_fast(true);
        if (null !== $feed->getUserAgent()) {
            $feedReader->set_useragent($feed->getUserAgent());
        }
        $feedReader->set_curl_options([
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
        ]);
        $feedReader->init();

        if (null !== $feedReader->error()) {
            throw new FeedException(is_array($feedReader->error()) ? implode(' - ', $feedReader->error()) : $feedReader->error());
        }

        $titleText = new SimpleString($feedReader->get_title());
        $titleText->removeEmojis();
        $descriptionText = new SimpleString($feedReader->get_description());
        $descriptionText->removeEmojis();

        $feed->setTitle($titleText->getText());
        $feed->setDescription($descriptionText->getText());

        $items = $feedReader->get_items();

        if (null !== $items) {
            foreach ($items as $item) {
                if ($this->importFeedItem($item, $feed)) {
                    $itemCount++;
                }
            }

            $feed->setLastImport(new DateTime());
        }

        $this->entityAdministratorManager->get(Feed::class)->save($feed);

        return $itemCount;
    }

    /**
     * Import the item and save it if not already saved.
     *
     * @param \SimplePie_Item  $item The item
     * @param \App\Entity\Feed $feed The feed
     *
     * @return bool If an item has been saved
     */
    private function importFeedItem(SimplePie_Item $item, Feed $feed)
    {
        $title = $item->get_title();
        $feedDate = $item->get_date(DateTime::ISO8601);
        $date = null !== $feedDate ? DateTime::createFromFormat(DateTime::ISO8601, $feedDate) : new DateTime();

        if (null !== $title && null !== $date && !$feed->isDateBeforeLastPast($date)) {
            $titleText = new SimpleString($title);
            $titleText->removeEmojis();
            $descriptionText = new SimpleString($item->get_description());
            $descriptionText->removeEmojis();

            $enclosure = $item->get_enclosure();
            $feedItem = $this->entityAdministratorManager->get(FeedItem::class)->create([
                'title' => $titleText->getText(),
                'description' => $descriptionText->getText(),
                'url' => $item->get_link(),
                'image' => (null !== $enclosure ? $enclosure->get_thumbnail() : null),
                'date' => $date
            ]);
            $feed->addItem($feedItem);
            return true;
        }

        return false;
    }
}
