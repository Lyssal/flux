<?php
namespace App\Twig\Extension;

use App\Entity\Category;
use Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Extension Twig to have feed categories.
 */
class CategoryExtension extends AbstractExtension
{
    protected $entityAdministratorManager;

    public function __construct(EntityAdministratorManager $entityAdministratorManager)
    {
        $this->entityAdministratorManager = $entityAdministratorManager;
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
       return [
       	   new TwigFunction('app_category_parents', [$this, 'getParentCategories']),
       ];
    }

    /**
     * Get the parent catégories.
     */
    public function getParentCategories()
    {
        return $this->entityAdministratorManager->get(Category::class)->findBy([
            'parent' => null
        ], [
            'position'
        ]);
    }
}
