<?php
namespace App\Command;

use App\Entity\Feed;
use App\Exception\FeedException;
use App\Feed\FeedImport;
use Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command which save all new feed items.
 */
class ImportCommand extends Command
{
    private $entityAdministratorManager;

    /**
     * @var \App\Feed\FeedImport The feed import
     */
    private $feedImport;

    public function __construct(EntityAdministratorManager $entityAdministratorManager, FeedImport $feedImport)
    {
        parent::__construct();

        $this->entityAdministratorManager = $entityAdministratorManager;
        $this->feedImport = $feedImport;
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:feed:import')
            ->setDescription('Import all the new feed items')
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $feeds = $this->entityAdministratorManager->get(Feed::class)->findAll();

        foreach ($feeds as $feed) {
            $feedTitle = (null !== $feed->getTitle() ? $feed->getTitle() : '<fg=yellow>Titre inconnu</>');

            try {
                $savedItemCount = $this->feedImport->importFeed($feed);
            } catch (FeedException $e) {
                $savedItemCount = 0;
                $output->writeln('<fg=red;options=bold>'.$feedTitle.' : '.$e->getMessage().'</>');
            }

            if ($savedItemCount > 0) {
                $output->writeln($feedTitle.' :  <fg=green;options=bold>'.$savedItemCount.'</> item'.($savedItemCount > 1 ? 's' : '').' saved.');
            }
        }
    }
}
