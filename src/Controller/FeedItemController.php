<?php
namespace App\Controller;

use App\Entity\FeedItem;
use Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for feed items.
 *
 * @Route("/FeedItem")
 */
class FeedItemController extends AbstractController
{
    /**
     * View a feed item.
     *
     * @Route("/{feedItem}/Read/{read}", name="flux_feeditem_read")
     *
     * @return Response
     */
    public function read(EntityAdministratorManager $entityAdministratorManager, FeedItem $feedItem, $read)
    {
        $feedItem->setRead($read);
        $entityAdministratorManager->get(FeedItem::class)->save($feedItem);

        return $this->render('feed_item/read.html.twig', [
            'item' => $feedItem
        ]);
    }

    /**
     * Relove a feed item.
     *
     * @Route("/{feedItem}/Remove", name="flux_feeditem_remove")
     *
     * @return Response
     */
    public function remove(EntityAdministratorManager $entityAdministratorManager, FeedItem $feedItem)
    {
        $entityAdministratorManager->get(FeedItem::class)->delete($feedItem);

        return new Response('');
    }
}
