<?php
namespace App\Controller;

use App\Entity\Feed;
use App\Entity\FeedItem;
use Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for homepage.
 */
class IndexController extends AbstractController
{
    /**
     * The homepage.
     *
     * @Route("/")
     *
     * @return Response
     */
    public function view(EntityAdministratorManager $entityAdministratorManager)
    {
        return $this->render('index/view.html.twig', [
            'feedCount' => $entityAdministratorManager->get(Feed::class)->count(),
            'unreadFeedItemCount' => $entityAdministratorManager->get(FeedItem::class)->count(['read' => false]),
        ]);
    }
}
